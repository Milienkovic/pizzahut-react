import React, {Fragment, useEffect} from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import {AppUrls} from "./utils/appUrls";
import Home from "./ui/pages/Home/Home";
import Login from "./ui/components/auth/Login/Login";
import Registration from "./ui/components/auth/Registration/Registration";
import Navigation from "./ui/components/commons/Navigation/Navigation";
import {useToken} from "./utils/hooks/useToken";
import {useDispatch, useSelector} from "react-redux";
import {loginSuccess, logout} from "./store/actions/auth/authActions";
import AnonymousRoute from "./ui/components/commons/routes/AnonymousRoute";

import './App.css';
import CartOverview from "./ui/pages/CartOverview/CartOverview";
import Checkout from "./ui/components/Checkout/Checkout";
import AuthenticatedRoute from "./ui/components/commons/routes/AuthenticatedRoute";
import OrdersPreview from "./ui/pages/OrdersPreview/OrdersPreview";

function App() {

    const {isTokenStored, isTokenValid, getAuthToken} = useToken();
    const dispatch = useDispatch();
    const {isAuth} = useSelector(state => ({
        isAuth: !!state.auth.token
    }));

    useEffect(() => {
        if (!isAuth && isTokenStored) {
            const token = getAuthToken();
            if (isTokenValid(token)) {
                dispatch(loginSuccess(token));
            } else {
                dispatch(logout());
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Fragment>
            <Navigation/>
            <main className={'App'}>
                <Switch>
                    <Route exact path={AppUrls.HOME} component={Home}/>
                    <AnonymousRoute exact path={AppUrls.LOGIN} component={Login}/>
                    <AnonymousRoute exact path={AppUrls.REGISTRATION} component={Registration}/>
                    <Route exact path={AppUrls.CART} component={CartOverview}/>
                    <Route exact path={AppUrls.CHECKOUT} component={Checkout}/>
                    <AuthenticatedRoute exact path={AppUrls.ORDERS} component={OrdersPreview}/>
                    <Redirect to={AppUrls.HOME}/>
                </Switch>
            </main>
        </Fragment>
    );
}

export default App;
