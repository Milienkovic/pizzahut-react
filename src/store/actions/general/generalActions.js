import {GeneralTypes} from "./generalTypes";
import axios from 'axios';
import {Endpoints} from "../../../utils/endpoints";
import {actionFail, actionStart, actionSuccess} from "../utils/actions";


const fetchRateStart = () => actionStart(GeneralTypes.FETCH_EXCHANGE_RATE_START);
const fetchRateFail = error => actionFail(GeneralTypes.FETCH_EXCHANGE_RATE_FAIL, error);
const fetchRateSuccess = rate => actionSuccess(GeneralTypes.FETCH_EXCHANGE_RATE_SUCCESS, 'rate', rate);
export const purge = () => {
    return {
        type: GeneralTypes.PURGE_ERRORS
    }
}

export const fetchExchangeRate = () => {
    return dispatch => {
        dispatch(fetchRateStart());
        axios.get(Endpoints.External.exchangeRate())
            .then(res => {
                dispatch(fetchRateSuccess(res.data.rates.USD));
            })
            .catch(err => {
                dispatch(fetchRateFail('Fetching failed!'));
            });
    }
}
