import {actionFail, actionStart, actionSuccess} from "../utils/actions";
import {OrderTypes} from "./orderTypes";
import axios from '../../../utils/axiosInstance';
import {Endpoints} from "../../../utils/endpoints";

const fetchOrderStart = () => actionStart(OrderTypes.FETCH_ORDER_START);
const fetchOrderFail = error => actionFail(OrderTypes.FETCH_ORDER_FAIL, error);
const fetchOrderSuccess = order => actionSuccess(OrderTypes.FETCH_ORDER_SUCCESS, 'order', order);

const fetchCustomerOrdersStart = () => actionStart(OrderTypes.FETCH_ORDER_START);
const fetchCustomerOrdersFail = error => actionFail(OrderTypes.FETCH_ORDER_FAIL, error);
const fetchCustomerOrdersSuccess = orders => actionSuccess(OrderTypes.FETCH_CUSTOMER_ORDERS_SUCCESS, 'orders', orders);

const mutateOrderStart = () => actionStart(OrderTypes.MUTATE_ORDER_START);
const mutateOrderFail = (error, fieldErrors) => actionFail(OrderTypes.MUTATE_ORDER_FAIL, error, fieldErrors);
const createOrderSuccess = order => actionSuccess(OrderTypes.CREATE_ORDER_SUCCESS, 'order', order);
const deleteOrderSuccess = id => actionSuccess(OrderTypes.DELETE_ORDER_SUCCESS, 'id', id);


const fetchOrder = orderId => {
    return dispatch => {
        dispatch(fetchOrderStart());
        axios.get(Endpoints.Order.fetchOrder(orderId))
            .then(res => {
                dispatch(fetchOrderSuccess(res.data));
            })
            .catch(err => {
                dispatch(fetchOrderFail(err.response.data.message || 'Checkout fetching failed!'));
            });
    }
}

export const fetchCustomerOrders = userId => {
    return dispatch => {
        dispatch(fetchCustomerOrdersStart());
        axios.get(Endpoints.Order.fetchCustomerOrders(userId))
            .then(res => {
                dispatch(fetchCustomerOrdersSuccess(res.data));
            })
            .catch(err => {
                dispatch(fetchCustomerOrdersFail(err.response.data.message || 'Fetching failed!'));
            });
    }
}

export const createOrder = (customerId, orderData) => {
    return dispatch => {
        dispatch(mutateOrderStart());
        axios.post(Endpoints.Order.createOrder(customerId), orderData)
            .then(res => {
                dispatch(createOrderSuccess(res.data));
            })
            .catch(err => {
                dispatch(mutateOrderFail(err.response.data.message || 'Checkout creation failed!', err.response.data.errors))
            });
    }
}

export const deleteOrder = orderId => {
    return dispatch => {
        dispatch(mutateOrderStart());
        axios.delete(Endpoints.Order.deleteOrder(orderId))
            .then(res => {
                console.log(res.data);
                dispatch(deleteOrderSuccess(orderId));
            })
            .catch(err => {
                dispatch(mutateOrderFail(err.response.data.message || 'Checkout deletion failed!'));
            });
    }
}
