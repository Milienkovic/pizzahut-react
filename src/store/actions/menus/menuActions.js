import axios from '../../../utils/axiosInstance';
import {actionFail, actionStart, actionSuccess} from "../utils/actions";
import {MenuTypes} from "./menuTypes";
import {Endpoints} from "../../../utils/endpoints";

const fetchMenuStart = () => actionStart(MenuTypes.FETCH_MENU_START);
const fetchMenuFail = error => actionFail(MenuTypes.FETCH_MENU_FAIL, error);
const fetchMenuSuccess = menu => actionSuccess(MenuTypes.FETCH_MENU_SUCCESS, 'menu', menu);

const changeCurrencySuccess = currency => actionSuccess(MenuTypes.CHANGE_CURRENCY, 'currency', currency);
export const fetchMenu = () => {
    return dispatch => {
        dispatch(fetchMenuStart());
        axios.get(Endpoints.Menu.fetchMenu())
            .then(res => {
                dispatch(fetchMenuSuccess(res.data));
            })
            .catch(err => {
                dispatch(fetchMenuFail(err.response.data.message || 'Failed to fetch menu!'));
            });
    }
}

export const changeCurrency = currency =>{
    return dispatch =>{
        dispatch(changeCurrencySuccess(currency));
    }
}
