export class AuthTypes {
    static AUTH_START = 'AUTH_START';
    static REGISTRATION_SUCCESS = 'REGISTRATION_SUCCESS';
    static LOGIN_SUCCESS = 'LOGIN_SUCCESS';
    static LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
    static AUTH_FAIL = 'AUTH_FAIL';
}
