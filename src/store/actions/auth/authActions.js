import {actionFail, actionStart, actionSuccess} from "../utils/actions";
import {AuthTypes} from "./authTypes";
import axios from '../../../utils/axiosInstance';
import {Endpoints} from "../../../utils/endpoints";
import {AppUrls} from "../../../utils/appUrls";
import {clearCart} from "../cart/cartActions";

const TOKEN = 'auth-token';
const authStart = () => actionStart(AuthTypes.AUTH_START);
const authFail = (error, fieldErrors) => actionFail(AuthTypes.AUTH_FAIL, error, fieldErrors);
export const loginSuccess = token => actionSuccess(AuthTypes.LOGIN_SUCCESS, 'token', token);
const registrationSuccess = token => actionSuccess(AuthTypes.REGISTRATION_SUCCESS, 'token', token);
const logoutSuccess = () => actionSuccess(AuthTypes.LOGOUT_SUCCESS, 'token', null);


export const login = (loginData, history) => {
    return dispatch => {
        dispatch(authStart());
        axios.post(Endpoints.Auth.login(), loginData)
            .then(res => {
                console.log(res);
                const {authorization} = res.headers;
                console.log(authorization);
                localStorage.setItem(TOKEN, authorization);
                dispatch(loginSuccess(authorization));
                history.push(AppUrls.HOME);
            })
            .catch(err => {
                console.log(err.response.data);
                dispatch(authFail(err.response.data.message || 'Login Failed!', err.response.data.errors));
            });
    }
}

export const registration = (registrationData, history) => {
    return dispatch => {
        dispatch(authStart());
        axios.post(Endpoints.Auth.registration(), registrationData)
            .then(res => {
                console.log(res);
                const {authorization} = res.headers;
                localStorage.setItem(TOKEN, authorization);
                dispatch(registrationSuccess(authorization));
                history.push(AppUrls.HOME);
            })
            .catch(err => {
                console.log(err.response.data);
                dispatch(authFail(err.response.data.message || 'Registration Failed!', err.response.data[0].errors));
            });
    }
}

export const logout = () => {
    return dispatch => {
        localStorage.removeItem(TOKEN);
        dispatch(logoutSuccess());
        dispatch(clearCart());
    }
}
