import axios from '../../../utils/axiosInstance';
import {actionFail, actionStart, actionSuccess} from "../utils/actions";
import {CustomerTypes} from "./customerTypes";
import {Endpoints} from "../../../utils/endpoints";
import {AppUrls} from "../../../utils/appUrls";
import {clearCart} from "../cart/cartActions";

const fetchCustomerStart = () => actionStart(CustomerTypes.FETCH_CUSTOMER_START);
const fetchCustomerFail = error => actionFail(CustomerTypes.FETCH_CUSTOMER_FAIL, error);
const fetchCustomerSuccess = customer => actionSuccess(CustomerTypes.FETCH_CUSTOMER_SUCCESS, 'customer', customer);

const mutateCustomerStart = () => actionStart(CustomerTypes.MUTATE_CUSTOMER_START);
const createCustomerSuccess = customer => actionSuccess(CustomerTypes.CREATE_CUSTOMER_SUCCESS, 'customer', customer);
const deleteCustomerSuccess = id => actionSuccess(CustomerTypes.DELETE_CUSTOMER_SUCCESS, 'id', id);
const mutateCustomerFail = (error, fieldErrors) => actionFail(CustomerTypes.MUTATE_CUSTOMER_FAIL, error, fieldErrors);

export const fetchCustomer = userId => {
    return dispatch => {
        dispatch(fetchCustomerStart());
        axios.get(Endpoints.Customer.fetchCustomer(userId))
            .then(res => {
                dispatch(fetchCustomerSuccess(res.data));
            })
            .catch(err => {
                dispatch(fetchCustomerFail(err.response.data.message || 'Customer fetching failed!'));
            });
    }
}

export const createCustomer = (customerData, history) => {
    return dispatch => {
        dispatch(mutateCustomerStart());
        axios.post(Endpoints.Customer.createCustomer(), customerData)
            .then(res => {
                dispatch(createCustomerSuccess(res.data));
                dispatch(clearCart());
                history.push(AppUrls.HOME);
            })
            .catch(err => {
                dispatch(mutateCustomerFail(err.response.data.message || 'Customer creation failed!', err.response.data.errors));
            });
    }
}

export const deleteCustomer = customerId => {
    return dispatch => {
        dispatch(mutateCustomerStart());
        axios.delete(Endpoints.Customer.deleteCustomer(customerId))
            .then(res => {
                dispatch(deleteCustomerSuccess(customerId));
            })
            .catch(err => {
                dispatch(mutateCustomerFail(err.response.data.message || 'Customer deletion failed!'));
            });
    }
}
