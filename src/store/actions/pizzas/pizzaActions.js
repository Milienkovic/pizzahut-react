import {actionFail, actionStart, actionSuccess} from "../utils/actions";
import {PizzaTypes} from "./pizzaTypes";
import axios from '../../../utils/axiosInstance';
import {Endpoints} from "../../../utils/endpoints";

const fetchPizzasStart = () => actionStart(PizzaTypes.FETCH_PIZZA_START);
const fetchPizzasFail = error => actionFail(PizzaTypes.FETCH_PIZZA_FAIL, error);
const fetchPizzasSuccess = pizzas => actionSuccess(PizzaTypes.FETCH_PIZZAS_SUCCESS, 'pizzas', pizzas);

const fetchOrderPizzasStart = () => actionStart(PizzaTypes.FETCH_PIZZA_START);
const fetchOrderPizzasSuccess = pizzas => actionSuccess(PizzaTypes.FETCH_ORDERS_PIZZAS_SUCCESS, 'pizzas', pizzas);
const fetchOrderPizzasFail = error => actionFail(PizzaTypes.FETCH_PIZZA_FAIL, error);

const fetchPizzaStart = () => actionStart(PizzaTypes.FETCH_PIZZA_START);
const fetchPizzaSuccess = pizza => actionSuccess(PizzaTypes.FETCH_PIZZA_SUCCESS, 'pizza', pizza);
const fetchPizzaFail = error => actionFail(PizzaTypes.FETCH_PIZZA_FAIL, error);

const mutatePizzaStart = () => actionStart(PizzaTypes.MUTATE_PIZZA_START);
const createPizzaSuccess = pizza => actionSuccess(PizzaTypes.CREATE_PIZZA_SUCCESS, 'pizza', pizza);
const deletePizzaSuccess = pizzaId => actionSuccess(PizzaTypes.DELETE_PIZZA_SUCCESS, 'id', pizzaId);
const mutatePizzaFail = (error, fieldErrors) => actionFail(PizzaTypes.MUTATE_PIZZA_FAIL, error, fieldErrors);

export const fetchPizzas = menuId => {
    return dispatch => {
        dispatch(fetchPizzasStart());
        axios.get(Endpoints.Pizza.fetchPizzas(menuId))
            .then(res => {
                dispatch(fetchPizzasSuccess(res.data));
            })
            .catch(err => {
                dispatch(fetchPizzasFail(err.response.data.message || 'Pizzas fetching failed!'));
            });
    }
}

export const fetchOrderPizzas = orderId => {
    return dispatch => {
        dispatch(fetchOrderPizzasStart());
        axios.get(Endpoints.Pizza.fetchOrdersPizzas(orderId))
            .then(res => {
                dispatch(fetchOrderPizzasSuccess(res.data));
            })
            .catch(err => {
                dispatch(fetchOrderPizzasFail(err.response.data.message || 'Pizzas fetching failed!'));
            });
    }
}

export const fetchPizza = pizzaId => {
    return dispatch => {
        dispatch(fetchPizzaStart());
        axios.get(Endpoints.Pizza.fetchPizza(pizzaId))
            .then(res => {
                dispatch(fetchPizzaSuccess(res.data));
            })
            .catch(err => {
                dispatch(fetchPizzaFail(err.response.data.message || 'Pizzas fetching failed!'));
            });
    }
}

export const createPizza = (menuId, pizzaData) => {
    return dispatch => {
        dispatch(mutatePizzaStart());
        axios.post(Endpoints.Pizza.createPizza(menuId), pizzaData)
            .then(res => {
                dispatch(createPizzaSuccess(res.data));
            })
            .catch(err => {
                dispatch(mutatePizzaFail(err.response.data.message || 'Pizzas deletion failed!', err.response.data.errors));
            });
    }
}

export const deletePizza = pizzaId => {
    return dispatch => {
        dispatch(mutatePizzaStart());
        axios.delete(Endpoints.Pizza.deletePizza(pizzaId))
            .then(res => {
                dispatch(deletePizzaSuccess(pizzaId));
            })
            .catch(err => {
                dispatch(mutatePizzaFail(err.response.data.message || 'Pizzas deletion failed!'));
            });
    }
}
