import {CartTypes} from "./cartTypes";

export const addCart = cartItem => {
    return{
        type: CartTypes.ADD_CART,
        pizza: cartItem
    }
}

export const removeCart = cartItemId => {
    return{
        type: CartTypes.REMOVE_CART,
        pizzaId: cartItemId
    }
}

export const updateCart = cartItem => {
    return{
        type: CartTypes.UPDATE_CART,
        pizza: cartItem
    }
}

export const clearCart = () => {
    return {
        type: CartTypes.CLEAR_CART
    }
}
