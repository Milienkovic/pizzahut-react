export class CartTypes {
    static ADD_CART='ADD_CART';
    static REMOVE_CART='REMOVE_CART';
    static UPDATE_CART ='UPDATE_CART';
    static CLEAR_CART='CLEAR_CART';
}
