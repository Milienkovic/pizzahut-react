export const actionStart = type => {
    return {
        type
    }
}

export const actionFail = (type, error, fieldErrors = null) => {
    return {
        type,
        error,
        fieldErrors
    }
}

export const actionSuccess = (type, propName, propValue) => {
    return {
        type,
        [propName]: propValue
    }
}
