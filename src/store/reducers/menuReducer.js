import {MenuTypes} from "../actions/menus/menuTypes";
import {GeneralTypes} from "../actions/general/generalTypes";
import {actionFail, actionStart} from "./utils/reducerActions";

const initState = {
    loading: false,
    menu: null,
    error: null,
    rate: 1.14,
    currency: 'EUR'
}

const fetchMenuStart = (state, action) => actionStart(state, action);
const fetchMenuSuccess = (state, action) => {
    return {
        ...state,
        menu: action.menu,
        loading: false
    }
};

const fetchExchangeRateSuccess = (state, action) => {
    return {
        ...state,
        rate: action.rate,
        loading: false
    }
}

const changeCurrencySuccess = (state, action) => {
    return {
        ...state,
        loading: false,
        currency: action.currency
    }
}

const fetchMenuFail = (state, action) => actionFail(state, action);

export function menuReducer(state = initState, action) {
    switch (action.type) {
        case MenuTypes.FETCH_MENU_START:
            return fetchMenuStart(state, action);
        case MenuTypes.FETCH_MENU_SUCCESS:
            return fetchMenuSuccess(state, action);
        case MenuTypes.FETCH_MENU_FAIL:
            return fetchMenuFail(state, action);
        case GeneralTypes.FETCH_EXCHANGE_RATE_SUCCESS:
            return fetchExchangeRateSuccess(state, action);
        case MenuTypes.CHANGE_CURRENCY:
            return changeCurrencySuccess(state, action);
        default:
            return state;
    }
}

