import {CartTypes} from "../actions/cart/cartTypes";

const initState = {
    pizzas: [],
    total: 0,
    itemsCount: 0,
    deliveryCost: 0,
    deliveryIncluded: false
}

const addCart = (state, action) => {
    const pizza = state.pizzas.find(pizza => pizza.id === action.pizza.id);
    const deliveryCost = +action.pizza.deliveryCost;

    if (pizza) {
        pizza.amount += action.pizza.amount;
        let total;
        if (state.deliveryIncluded) {
            total = state.total + pizza.price * action.pizza.amount;
        } else {
            total = state.total + pizza.price * action.pizza.amount + deliveryCost;
        }
        return {
            ...state,
            pizzas: state.pizzas.filter(p => p.id === action.pizza.id ? pizza : p),
            total,
            itemsCount: state.itemsCount + action.pizza.amount,
            deliveryCost,
            deliveryIncluded: true
        }

    } else {
        const pizzas = [...state.pizzas, action.pizza];
        let total;
        if (state.deliveryIncluded) {
            total = state.total + action.pizza.price * action.pizza.amount;
        } else {
            total = state.total + action.pizza.price * action.pizza.amount + deliveryCost;
        }
        const itemsCount = state.itemsCount + action.pizza.amount;
        return {
            ...state,
            pizzas,
            total,
            itemsCount,
            deliveryCost,
            deliveryIncluded: true
        }
    }
}

const updateCart = (state, action) => {
    const pizza = state.pizzas.find(pizza => pizza.id === action.pizza.id);
    if (pizza) {
        return {
            pizzas: state.pizzas.filter(p => p.id === action.pizza.id ? action.pizza : p),
            total: state.total - pizza.price * pizza.amount + action.pizza.price * action.pizza.amount,
            itemsCount: state.itemsCount - pizza.amount + action.pizza.amount,
        }
    } else {
        return state;
    }

}

const removeCart = (state, action) => {
    const pizza = state.pizzas.find(pizza => pizza.id === action.pizzaId);
    let total;
    let itemsCount;
    if (pizza) {
        total = state.total - pizza.price * pizza.amount;
        itemsCount = state.itemsCount - pizza.amount;
        return {
            ...state,
            pizzas: state.pizzas.filter(pizza => pizza.id !== action.pizzaId),
            total,
            itemsCount,
        }
    }
    return state;
}

const clearCart = () => {
    return initState;
}

export const cartReducer = (state = initState, action) => {
    switch (action.type) {
        case CartTypes.ADD_CART:
            return addCart(state, action);
        case CartTypes.UPDATE_CART:
            return updateCart(state, action);
        case CartTypes.REMOVE_CART:
            return removeCart(state, action);
        case CartTypes.CLEAR_CART:
            return clearCart();
        default:
            return state;
    }
}
