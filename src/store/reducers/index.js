import {combineReducers} from "redux";
import authReducer from "./authReducer";
import {menuReducer} from "./menuReducer";
import {cartReducer} from "./cartReducer";
import orderReducer from "./orderReducer";
import customerReducer from "./customerReducer";

export const rootReducer = combineReducers({
    auth: authReducer,
    menus: menuReducer,
    carts: cartReducer,
    orders: orderReducer,
    customers: customerReducer
});
