export const actionStart = (state, action) => {
    return {
        ...state,
        loading: true,
        error: null,
        fieldErrors: null
    };
};

export const actionFail = (state, action) => {
    return {
        ...state,
        loading: false,
        error: action.error,
        fieldErrors: action.fieldErrors
    };
};

export const purge = (state, action) => {
    return {
        ...state,
        loading: false,
        error: null,
        fieldErrors: null
    };
};
