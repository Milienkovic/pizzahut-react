import {AuthTypes} from "../actions/auth/authTypes";
import {actionFail, actionStart, purge} from "./utils/reducerActions";
import {GeneralTypes} from "../actions/general/generalTypes";

const initState = {
    loading: false,
    error: null,
    fieldErrors: null,
    token: null,
}

const authStart = (state, action) => actionStart(state, action);
const authFail = (state, action) => actionFail(state, action);
const authSuccess = (state, action) => {
    return {
        ...state,
        loading: false,
        token: action.token || null
    }
}
const loginSuccess = (state, action) => authSuccess(state, action);

const registrationSuccess = (state, action) => authSuccess(state, action);

const logoutSuccess = (state, action) => authSuccess(state, action);

const purgeErrors = (state, action) => purge(state, action);

export default function authReducer(state = initState, action) {
    switch (action.type) {
        case AuthTypes.AUTH_START:
            return authStart(state, action);
        case AuthTypes.LOGIN_SUCCESS:
            return loginSuccess(state, action);
        case AuthTypes.REGISTRATION_SUCCESS:
            return registrationSuccess(state, action);
        case AuthTypes.LOGOUT_SUCCESS:
            return logoutSuccess(state, action);
        case AuthTypes.AUTH_FAIL:
            return authFail(state, action);
        case GeneralTypes.PURGE_ERRORS:
            return purgeErrors(state, action);
        default:
            return state;
    }
}
