import {CustomerTypes} from "../actions/customers/customerTypes";
import {actionFail, actionStart, purge} from "./utils/reducerActions";
import {GeneralTypes} from "../actions/general/generalTypes";

const initState = {
    loading: false,
    error: null,
    fieldErrors: null,
    customer: null,
    hasCustomer: false
}

const fetchCustomerStart = (state, action) => actionStart(state, action);
const fetchCustomerSuccess = (state, action) => {
    return {
        ...state,
        loading: false,
        customer: action.customer,
        hasCustomer: true
    }
}

const fetchCustomerFail = (state, action) => actionFail(state, action);

const mutateCustomerStart = (state, action) => actionStart(state, action);
const createCustomerSuccess = (state, action) => {
    return {
        ...state,
        loading: false,
        customer: action.customer
    }
}
const mutateCustomerFail = (state, action) => actionFail(state, action);

const purgeErrors = (state, action) => purge(state, action);

const customerReducer = (state = initState, action) => {
    switch (action.type) {
        case CustomerTypes.FETCH_CUSTOMER_START:
            return fetchCustomerStart(state, action);
        case CustomerTypes.FETCH_CUSTOMER_SUCCESS:
            return fetchCustomerSuccess(state, action);
        case CustomerTypes.FETCH_CUSTOMER_FAIL:
            return fetchCustomerFail(state, action);
        case CustomerTypes.MUTATE_CUSTOMER_START:
            return mutateCustomerStart(state, action);
        case CustomerTypes.CREATE_CUSTOMER_SUCCESS:
            return createCustomerSuccess(state, action);
        case CustomerTypes.MUTATE_CUSTOMER_FAIL:
            return mutateCustomerFail(state, action);
        case GeneralTypes.PURGE_ERRORS:
            return purgeErrors(state, action)
        default:
            return state;
    }
}

export default customerReducer;
