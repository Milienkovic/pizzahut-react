import {GeneralTypes} from "../actions/general/generalTypes";
import {OrderTypes} from "../actions/orders/orderTypes";
import {actionFail, actionStart, purge} from "./utils/reducerActions";

const initState = {
    loading: false,
    error: null,
    fieldErrors: null,
    order: null
}

const fetchOrderStart = (state, action) => actionStart(state, action);
const fetchOrderSuccess = (state, action) => {
    return {
        ...state,
        loading: false,
        order: action.order
    }
}

const fetchCustomerOrdersSuccess = (state, action) => {
    return {
        ...state,
        loading: false,
        orders: action.orders
    }
}

const mutateOrderStart = (state, action) => actionStart(state, action);
const createOrderSuccess = (state, action) => {
    return {
        ...state,
        loading: false,
        order: action.order
    }
}
const mutateOrderFail = (state, action) => actionFail(state, action);

const fetchOrderFail = (state, action) => actionFail(state, action);

const purgeErrors = (state, action) => purge(state, action);

export default function orderReducer (state = initState, action) {
    switch (action.type) {
        case OrderTypes.FETCH_ORDER_START:
            return fetchOrderStart(state, action);
        case OrderTypes.FETCH_ORDER_SUCCESS:
            return fetchOrderSuccess(state, action);
        case OrderTypes.FETCH_CUSTOMER_ORDERS_SUCCESS:
            return fetchCustomerOrdersSuccess(state, action);
        case OrderTypes.FETCH_ORDER_FAIL:
            return fetchOrderFail(state, action);
        case OrderTypes.MUTATE_ORDER_START:
            return mutateOrderStart(state, action);
        case OrderTypes.CREATE_ORDER_SUCCESS:
            return createOrderSuccess(state, action);
        case OrderTypes.MUTATE_ORDER_FAIL:
            return mutateOrderFail(state, action);
        case GeneralTypes.PURGE_ERRORS:
            return purgeErrors(state, action);
        default:
            return state;
    }
}

