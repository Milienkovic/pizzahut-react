export class AppUrls {
    static HOME = '/';
    static LOGIN = '/login';
    static LOGOUT = '/logout';
    static REGISTRATION = '/registration';
    static CART = '/cart';
    static CHECKOUT = '/checkout';
    static ORDERS = '/orders';

}
