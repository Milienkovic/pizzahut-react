import {useCallback, useReducer} from "react";

const INPUT_CHANGE = 'INPUT_CHANGE';
const SET_DATA = 'SET_DATA';

const formReducer = (state, action) => {
    switch (action.type) {
        case INPUT_CHANGE:
            return {
                ...state,
                inputs: {
                    ...state.inputs,
                    [action.inputId]: action.value
                }
            }
        case SET_DATA:
            return {
                inputs: action.formData
            }
        default:
            return state
    }
}

export const useForm = initialInputs => {
    const [formState, dispatch] = useReducer(formReducer, {
        inputs: initialInputs
    });

    const inputHandler = useCallback((inputId, value) => {
        dispatch({type: INPUT_CHANGE, inputId, value})
    }, []);

    const setFormData = useCallback(formData => {
        dispatch({type: SET_DATA, formData})
    }, []);

    return [formState, inputHandler, setFormData];
}
