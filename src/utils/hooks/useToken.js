import jwtDecode from 'jwt-decode';
import {useCallback} from "react";

const TOKEN = 'auth-token'

export const useToken = () => {
    const getAuthToken = useCallback(() => {
        return localStorage.getItem(TOKEN);
    }, []);

    const storeAuthToken = useCallback((token) => {
        if (token)
            localStorage.setItem(TOKEN, token);
    }, []);

    const decodeAuthToken = useCallback(token => {
        if (token)
            return jwtDecode(token);
        return undefined;
    }, []);

    const calculateExpiryDate = useCallback((expiresInMilisec) => {
        return new Date(new Date().getMilliseconds() + expiresInMilisec);
    }, []);

    const isTokenStored = useCallback(() => {
        return !!localStorage.getItem(TOKEN);
    }, []);

    const isTokenValid = useCallback(token => {
        if (token) {
            const decodedToken = jwtDecode(token);
            const now = Date.now();
            return decodedToken.exp > now / 1000;
        }
    },[]);

    return {storeAuthToken, getAuthToken, decodeAuthToken, calculateExpiryDate, isTokenValid, isTokenStored};
}
