import {useCallback} from "react";

export const useCurrency = () => {
    const calculateUSDPrice = useCallback((price, exchangeRate) => {
        return (price * exchangeRate).toFixed(2);
    }, []);

    const calculatePrice = useCallback((price, exchangeRate, currency) => {
        if (currency === 'EUR') {
            return price;
        } else if (currency === 'USD') {
            return +(price * exchangeRate);
        }
    }, []);

    return {calculateUSDPrice, calculatePrice};
}
