export class Endpoints {
    static Menu = class {
        static fetchMenu = () => `/api/menus`;
    }

    static Pizza = class {
        static fetchPizzas = menuId => `/api/${menuId}/pizzas`;
        static fetchPizza = pizzaId => `/api/pizzas/${pizzaId}`;
        static fetchOrdersPizzas = orderId => `/api/orders/${orderId}/pizzas`;
        static createPizza = menuId => `/api/${menuId}/pizzas`;
        static updatePizza = pizzaId => `/api/pizzas/${pizzaId}`;
        static deletePizza = pizzaId => `/api/pizzas/${pizzaId}`;
    }

    static Customer = class {
        static fetchCustomer = userId => `/api/customers/${userId}`;
        static fetchCustomers = () => '/api/customers';
        static createCustomer = () => '/api/customers';
        static updateCustomer = customerId => `/api/customers/${customerId}`;
        static deleteCustomer = customerId => `/api/customers/${customerId}`;
    }

    static Order = class {
        static fetchOrder = orderId => `/api/orders/${orderId}`;
        static fetchCustomerOrders = customerId => `/api/${customerId}/orders`;
        static createOrder = customerId => `/api/${customerId}/orders`;
        static updateOrder = orderId => `/api/orders/${orderId}`;
        static deleteOrder = orderId => `/api/orders/${orderId}`;
    }

    static Auth = class {
        static login = () => '/api/login';
        static registration = () => '/api/registration';
        static userDetails = () => '/api/details';
    }

    static External = class {
        static exchangeRate = () => 'https://api.exchangeratesapi.io/latest?symbols=USD';
    }
}
