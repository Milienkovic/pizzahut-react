import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';

import "./Modal.css";

const ModalOverlay = props => {
    const getClassNames = () => {
        const classNames = ['modal'];
        if (props.className)
            classNames.push(props.className)
        if (props.center)
            classNames.push('modal--content__center')
        return classNames;
    }

    const content = (
        <div className={getClassNames().join(' ')} style={props.style}>
            <header className={'modal__header'}>
                <h2>{props.header}</h2>
            </header>
            <div>
                <div className={`modal__content ${props.contentClass}`}>{props.children}</div>
                <footer className={`modal__footer ${props.footerClass}`}>
                    {props.footer}
                </footer>
            </div>
        </div>
    );
    return ReactDOM.createPortal(content, document.getElementById('modal-hook'));
}

const Modal = (props) => {

    const renderModal = () => {
        if (props.displayModal) {
            return (
                <Fragment>
                    <ModalOverlay {...props}/>
                </Fragment>
            )
        }
    };
    return (
        <Fragment>
            {renderModal()}
        </Fragment>
    );
};

export default Modal;
