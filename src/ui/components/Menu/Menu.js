import React, {Fragment, useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import Pizzas from "../Pizzas/Pizzas";
import Footer from "../commons/Footer/Footer";
import {fetchMenu} from "../../../store/actions/menus/menuActions";
import {fetchExchangeRate, purge} from "../../../store/actions/general/generalActions";
import CometSpinner from "../Spinner/CometSpinner";
import {AppUrls} from "../../../utils/appUrls";
import Cart from "../Cart/Cart";
import {fetchCustomer} from "../../../store/actions/customers/customerActions";
import {useToken} from "../../../utils/hooks/useToken";

import './Menu.css';

const Menu = () => {
    const {loading, error, menu, token} = useSelector(state => ({
        loading: state.menus.loading,
        error: state.menus.error,
        menu: state.menus.menu,
        exchangeRate: state.menus.rate,
        token: state.auth.token
    }));
    const {decodeAuthToken} = useToken();

    const dispatch = useDispatch();
    useEffect(() => {
        if (token) {
            const decoded = decodeAuthToken(token);
            const userId = decoded.sub;
            dispatch(fetchCustomer(userId));
        }
        return ()=>dispatch(purge());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token]);

    useEffect(() => {
        if (!menu) {
            dispatch(fetchMenu());
            dispatch(fetchExchangeRate());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    if (loading || !menu) {
        return <CometSpinner display={loading}/>
    }

    if (error) {
        return <h1>Please try later!</h1>
    }

    return (
        <Fragment>
            <Link to={AppUrls.CART}><Cart/></Link>
            <div className={'menu_container'}>
                <h1>Welcome to our Pizzeria!</h1>
                <h2>Place where the best pizzas are made</h2>
                <h1>{menu.name}</h1>
                <Pizzas pizzas={menu.pizzas}/>
            </div>
            <Footer/>
        </Fragment>
    );
};

export default Menu;
