import React, {Fragment} from 'react';
import Pizza from "./Pizza/Pizza";

const Pizzas = ({pizzas}) => {
    return (
        <Fragment>
            {pizzas.map(pizza => <Pizza
                key={pizza.id}
                pizza={pizza}
            />)}
        </Fragment>
    );
};

export default Pizzas;
