import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useCurrency} from "../../../../utils/hooks/useCurrency";
import {addCart, removeCart} from "../../../../store/actions/cart/cartActions";
import Button from "../../commons/form/Button/Button";
import Tooltip from "../../commons/Tooltip/Tooltip";
import {useForm} from "../../../../utils/hooks/useForm";

import './Pizza.css';

const initState = {
    amount: 1
}
const Pizza = props => {
    const {pizza} = props;
    const [formState, inputHandler] = useForm(initState);
    const [display, setDisplay] = useState(false);
    const {currency, exchangeRate, deliveryCost, cartPizzas} = useSelector(state => ({
        currency: state.menus.currency,
        exchangeRate: state.menus.rate,
        deliveryCost: state.menus.menu.delivery_cost,
        cartPizzas: state.carts.pizzas
    }));

    const {calculatePrice} = useCurrency();
    const dispatch = useDispatch();
    const pizzaPrice = parseFloat(calculatePrice(pizza.price, exchangeRate, currency)).toFixed(2);
    const addPizzaHandler = (pizza) => {
        const pizzaItem = {
            ...pizza,
            amount: +formState.inputs.amount,
            deliveryCost: deliveryCost
        }
        dispatch(addCart(pizzaItem));
    }

    const closeModalHandler = () => {
        setDisplay(false);
    }

    const removePizzaHandler = pizzaId => {
        dispatch(removeCart(pizzaId))
    }

    const renderRemoveCartButton = (pizzaId) => {
        const pizza = cartPizzas.find(pizza => pizza.id === pizzaId);
        if (pizza) {
            return <Button
                onClick={() => removePizzaHandler(pizza.id)}
                className={'cart_button'}
                danger>Remove</Button>
        }
    }

    return (
        <div className={'pizza_container'}>
            <div className={'pizza_inner-container'}
                 style={{
                     backgroundImage: `url(${require("../../../../assets/images/p-" + pizza.id + "-min.jpg")})`,
                     backgroundPosition: 'center',
                     backgroundSize: 'cover',
                     backgroundRepeat: 'no-repeat'
                 }}>
                <h1>{pizza.name}</h1>
                <h3>{pizza.description}</h3>
                <h5>{}</h5>
                <span className={'pizza_price'}>{currency === 'EUR' ? <span>&euro;</span> :
                    <span>$</span>}{pizzaPrice}</span>
                <Tooltip
                    display={display}
                    close={closeModalHandler}
                    onInput={inputHandler}
                    addCart={() => addPizzaHandler(pizza)}/>
                <div className={'pizza_actions'}>
                    <Button
                        onClick={() => setDisplay(true)}
                        className={'cart_button'}
                        shiny>Add to Cart
                    </Button>
                    {renderRemoveCartButton(pizza.id)}
                </div>
            </div>
        </div>
    );
};

export default Pizza;
