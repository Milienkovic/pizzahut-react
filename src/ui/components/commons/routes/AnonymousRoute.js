import React from 'react';
import {useSelector} from "react-redux";
import {Redirect, Route} from "react-router-dom";
import {AppUrls} from "../../../../utils/appUrls";

const AnonymousRoute = ({component: Component, ...otherProps}) => {
    const {isAuth} = useSelector(state => ({
        isAuth: !!state.auth.token
    }))
    return (
        <Route
            {...otherProps}
            render={props =>
                !isAuth ? <Component {...props}/>
                    : <Redirect to={AppUrls.HOME}/>
            }
        />

    );
};

export default AnonymousRoute;
