import React from 'react';
import Input from "../form/Input/Input";
import Button from "../form/Button/Button";

import './Tooltip.css';

const Tooltip = props => {

    const submitHandler = event => {
        event.preventDefault();
    }
    return (
        <div
            className={'tooltip_container'}>
            {props.children}
            {props.display && <div className={'tooltip_content'}>
                <form onSubmit={submitHandler}>
                    <Input
                        type={'number'}
                        min={1}
                        step={1}
                        name={'amount'}
                        id={'amount'}
                        label={'How many?'}
                        autoFocus
                        onInput={props.onInput}
                        initialValue={1}/>
                    <div className={'tooltip_actions'}>
                        <Button
                            type={'submit'}
                            onClick={() => {
                                props.addCart();
                                props.close();
                            }}
                            shiny>OK</Button>
                        <Button
                            onClick={props.close}
                            dark>X</Button>
                    </div>
                </form>
            </div>}

        </div>
    );
};

export default Tooltip;
