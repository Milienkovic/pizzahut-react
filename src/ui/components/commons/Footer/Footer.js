import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {changeCurrency} from "../../../../store/actions/menus/menuActions";
import {useCurrency} from "../../../../utils/hooks/useCurrency";
import EUFlag from "../../../../assets/images/eu-flag.png";
import USFlag from "../../../../assets/images/us-flag.png";

import './Footer.css';

const Footer = () => {

    const [currency, setCurrency] = useState('EUR');
    const dispatch = useDispatch();
    const {calculatePrice} = useCurrency();

    const {storedCurrency, deliveryCost, exchangeRate} = useSelector(store => ({
        storedCurrency: store.menus.currency,
        deliveryCost: store.menus.menu.delivery_cost,
        exchangeRate: store.menus.rate
    }));

    const delivery = parseFloat(calculatePrice(deliveryCost, exchangeRate, currency)).toFixed(2);
    useEffect(() => {
        if (storedCurrency !== currency)
            dispatch(changeCurrency(currency));
    }, [currency, dispatch, storedCurrency]);

    return (
        <footer className={'footer_container'}>
            <span>Delivery cost: {delivery}</span>{currency === 'EUR' ?
            <span>&euro;</span> :
            <span>$</span>}
            <div className={'currency_container'}>
                Chose currency:
                <div onClick={() => setCurrency('EUR')}><img src={EUFlag} alt={'EU'}/></div>
                <div onClick={() => setCurrency('USD')}><img src={USFlag} alt={'US'}/></div>
            </div>
        </footer>
    );
};

export default Footer;
