import React from 'react';
import {Link} from "react-router-dom";
import {AppUrls} from "../../../../utils/appUrls";
import Logo from '../../../../assets/images/logo.png';
import NavigationItems from "./NavigationItems/NavigationItems";
import './Navigation.css';

const Navigation = () => {
    return (
        <header className={'navigation_container'}>
            <div><Link to={AppUrls.HOME}><img src={Logo} alt={'logo'}/></Link></div>
            <div className={'spacer'}/>
            <nav>
                <NavigationItems/>
            </nav>
        </header>
    );
};

export default Navigation;
