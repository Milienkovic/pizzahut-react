import React from 'react';
import {NavLink} from "react-router-dom";

import './NavigationItem.css';

const NavigationItem = props => {
    return (
        <NavLink
            to={props.to}
            onClick={props.onClick}
            activeClassName={'active_navigation'}
            className={'navigation_item'}
            exact={props.exact}>
            {props.children}
        </NavLink>
    );
};

export default NavigationItem;
