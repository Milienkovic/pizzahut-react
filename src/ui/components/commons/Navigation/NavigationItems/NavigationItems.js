import React, {Fragment} from 'react';
import NavigationItem from "./NavigationItem/NavigationItem";
import {AppUrls} from "../../../../../utils/appUrls";
import {useDispatch, useSelector} from "react-redux";
import {logout} from "../../../../../store/actions/auth/authActions";

const NavigationItems = () => {
    const dispatch = useDispatch();
    const {isAuth} = useSelector(state => ({
        isAuth: !!state.auth.token
    }));

    const renderNavigation = () => {
        if (isAuth) {
            return <div>
                <NavigationItem
                    to={AppUrls.ORDERS}>
                    Orders History
                </NavigationItem>
                <NavigationItem
                    to={AppUrls.LOGOUT}
                    onClick={
                        () => {
                            dispatch(logout());
                            window.location.reload(false);
                        }
                    }
                >Logout</NavigationItem>
            </div>
        } else {
            return <div>
                <NavigationItem to={AppUrls.LOGIN}>Sign In</NavigationItem>
            </div>
        }
    }
    return (
        <Fragment>{renderNavigation()}</Fragment>
    );
};

export default NavigationItems;
