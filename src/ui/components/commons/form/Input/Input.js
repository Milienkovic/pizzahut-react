import React, {useEffect, useReducer} from 'react';
import './Input.css';

const CHANGE = 'CHANGE';

const inputReducer = (state, action) => {
    switch (action.type) {
        case CHANGE:
            return {
                ...state,
                value: action.value
            }
        default:
            return state;
    }
}

const Input = props => {
    const {id, name, autoFocus, fieldError, type, onInput, label} = props;
    const [inputState, dispatch] = useReducer(inputReducer, {
        value: props.initialValue || '',
    });

    const changeHandler = event => {
        const {value} = event.target;
        dispatch({type: CHANGE, value});
    }

    useEffect(() => {
        onInput(id, inputState.value);
    }, [id, inputState.value, onInput]);

    const classNames = () => {
        const className = ['input_label'];
        if (fieldError) {
            className.push('input_error_label');
        }
        return className;
    }


    return (
        <div className={'input_group'}>
            <label
                htmlFor={id}
                className={classNames().join(' ')}>
                {fieldError || label}
            </label>
            <input
                id={id}
                name={name}
                value={inputState.value}
                type={type}
                min={props.min}
                step={props.step}
                autoFocus={autoFocus}
                onChange={changeHandler}
                style={fieldError ? {borderBottom: '2px solid darkred'} : {}}
            />

        </div>
    );
};

export default Input;
