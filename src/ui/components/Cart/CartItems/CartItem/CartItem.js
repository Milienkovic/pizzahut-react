import React from 'react';
import {useSelector} from "react-redux";
import {useCurrency} from "../../../../../utils/hooks/useCurrency";

import './CartItem.css';

const CartItem = ({item}) => {
    const {currency, exchangeRate} = useSelector(state => ({
        currency: state.menus.currency,
        exchangeRate: state.menus.rate,
    }));

    const {calculatePrice} = useCurrency();
    const pizzaPrice = parseFloat(calculatePrice(item.price, exchangeRate, currency)).toFixed(2);
    const pizzasPrice = parseFloat(calculatePrice(item.price * item.amount, exchangeRate, currency)).toFixed(2);

    return (
        <div className={'cart_item-container'}>
            <span>{item.name}</span>
            <span>{pizzaPrice} x {item.amount}</span>
            <span>{pizzasPrice} {currency === 'EUR' ?
                <span>&euro;</span> : <span>$</span>}</span>
        </div>
    );
};

export default CartItem;
