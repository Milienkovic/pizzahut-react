import React from 'react';

import './CartItems.css';
import CartItem from "./CartItem/CartItem";

const CartItems = ({items}) => {
    return (
        <div className={'cart_items-container'}>
            <div className={'cart_items-inner-container'}>
                {items.map(item => <CartItem key={item.id} item={item}/>)}
            </div>
        </div>
    );
};

export default CartItems;
