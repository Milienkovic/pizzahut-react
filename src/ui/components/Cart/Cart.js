import React from 'react';
import {useSelector} from "react-redux";
import {useCurrency} from "../../../utils/hooks/useCurrency";
import CartImage from '../../../assets/images/cart.png';

import './Cart.css';

const Cart = () => {
    const {cartItemsCount, cartTotal, currency, exchangeRate} = useSelector(state => ({
        cartItemsCount: state.carts.itemsCount,
        cartTotal: state.carts.total.toFixed(2) || 0,
        currency: state.menus.currency,
        exchangeRate: state.menus.rate,
    }));

    const {calculatePrice} = useCurrency();
    const totalPrice = parseFloat(calculatePrice(cartTotal, exchangeRate, currency)).toFixed(2);


    return (
        <div className={'cart_container'}>
            <div className={'cart_inner-container'}>
                {/* eslint-disable-next-line jsx-a11y/img-redundant-alt */}
                <img src={CartImage} alt={'cart image'}/>
                {cartTotal > 0 &&
                <span className={'cart_total'}>{totalPrice}{currency === 'EUR' ? <span>&euro;</span> :
                    <span>$</span>}</span>}
                <span className={'cart_items'}>{cartItemsCount}</span>
                {/*<div*/}
                {/*    onClick={clearCartHandler}*/}
                {/*    className={'clear_cart'}>X*/}
                {/*</div>*/}
            </div>
        </div>
    );
};

export default Cart;
