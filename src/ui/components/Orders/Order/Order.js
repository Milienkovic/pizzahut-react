import React from 'react';

import './Order.css';

const Order = ({order}) => {
    const orderDate = new Date(order.created_at);
    const weekday = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

    const displayDate = orderDate.getMonth() + "-" + weekday[orderDate.getDay()] + "-" + orderDate.getFullYear() + ' '
        + orderDate.getHours() + ':' + orderDate.getMinutes();
    return (
        <div className={'order_container'}>
            <h4>{order.total} &euro;</h4>
            <span>{displayDate}</span>
        </div>
    );
};

export default Order;
