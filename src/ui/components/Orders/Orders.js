import React from 'react';
import Order from "./Order/Order";

import './ORders.css';
const Orders = props => {
    return (
        <div className={'orders_container'}>
            {props.orders.map(order=><Order key={ order.id} order={order}/>)}
        </div>
    );
};

export default Orders;
