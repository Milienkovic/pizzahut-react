import React, {useEffect} from 'react';
import Button from "../../commons/form/Button/Button";
import Input from "../../commons/form/Input/Input";
import {Link, useHistory} from "react-router-dom";
import {AppUrls} from "../../../../utils/appUrls";
import {useForm} from "../../../../utils/hooks/useForm";
import {useDispatch, useSelector} from "react-redux";
import {registration} from "../../../../store/actions/auth/authActions";
import {purge} from "../../../../store/actions/general/generalActions";
import CometSpinner from "../../Spinner/CometSpinner";

import './Registration.css';

const initState = {
    name: '',
    email: '',
    password: '',
    confirm_password: ''
}

const Registration = () => {
    const [formState, inputHandler] = useForm(initState);

    const dispatch = useDispatch();
    const history = useHistory();

    const {loading, error, fieldErrors} = useSelector(state => ({
        loading: state.auth.loading,
        error: state.auth.error,
        fieldErrors: state.auth.fieldErrors
    }))

    useEffect(() => {
        return () => {
            dispatch(purge());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const submitHandler = event => {
        event.preventDefault();
        dispatch(registration(formState.inputs, history));
    }

    if (loading) {
        return <CometSpinner display={loading}/>
    }

    return (
        <div className={'registration_container'}>
            <div className={'registration_inner-container'}>
                <h2>REGISTRATION</h2>
                {error && <h4 style={{color: 'darkred'}}>{error}</h4>}
                <form
                    className={'registration_form'}
                    onSubmit={submitHandler}>
                    <Input
                        type={'text'}
                        id={'name'}
                        name={'name'}
                        autoFocus
                        label={'Name: '}
                        initialValue={formState.inputs.name}
                        onInput={inputHandler}
                        fieldError={fieldErrors && fieldErrors.name && fieldErrors.name[0]}
                    />
                    <Input
                        type={'text'}
                        id={'email'}
                        name={'email'}
                        autoFocus
                        label={'Email: '}
                        initialValue={formState.inputs.email}
                        onInput={inputHandler}
                        fieldError={fieldErrors && fieldErrors.email && fieldErrors.email[0]}
                    />
                    <Input
                        type={'password'}
                        id={'password'}
                        name={'password'}
                        label={'Password: '}
                        initialValue={formState.inputs.password}
                        onInput={inputHandler}
                        fieldError={fieldErrors && fieldErrors.password && fieldErrors.password[0]}
                    />
                    <Input
                        type={'password'}
                        id={'confirm_password'}
                        name={'confirm_password'}
                        label={'Confirm Password: '}
                        initialValue={formState.inputs.confirm_password}
                        onInput={inputHandler}
                        fieldError={fieldErrors && fieldErrors.confirm_password && fieldErrors.confirm_password[0]}
                    />
                    <Button shiny type={'submit'}>Sign Up</Button>
                </form>
                <div className={'login_link_container'}>
                    <span>
                        Already have account? <Link to={AppUrls.LOGIN}>Sign In</Link>
                    </span>
                </div>
            </div>
        </div>
    );
};

export default Registration;
