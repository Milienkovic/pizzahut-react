import React, {useEffect} from 'react';
import Input from "../../commons/form/Input/Input";
import Button from "../../commons/form/Button/Button";
import {Link, useHistory} from "react-router-dom";
import {AppUrls} from "../../../../utils/appUrls";
import {useForm} from "../../../../utils/hooks/useForm";
import {useDispatch, useSelector} from "react-redux";
import {login} from "../../../../store/actions/auth/authActions";
import {purge} from "../../../../store/actions/general/generalActions";
import CometSpinner from "../../Spinner/CometSpinner";

import './Login.css';

const initState = {
    email: '',
    password: ''
}

const Login = () => {
    const [formState, inputHandler] = useForm(initState);

    const dispatch = useDispatch();
    const history = useHistory();

    const {error, fieldErrors, loading} = useSelector(state => ({
        loading: state.auth.loading,
        error: state.auth.error,
        fieldErrors: state.auth.fieldErrors,
    }));

    useEffect(() => {
        return () => {
            dispatch(purge());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const submitHandler = event => {
        event.preventDefault();
        dispatch(login(formState.inputs, history));
    }

    if (loading) {
        return <CometSpinner display={loading}/>
    }

    return (
        <div className={'login_form-container'}>
            <div className={'login_form-inner-container'}>
                <h2>LOGIN</h2>
                {error && <h4 className={'login_error'}>{error}</h4>}
                <form
                    className={'login_form'}
                    onSubmit={submitHandler}>
                    <Input
                        id={'email'}
                        name={'email'}
                        autoFocus
                        label={'Email: '}
                        type={'text'}
                        initialValue={formState.inputs.email}
                        onInput={inputHandler}
                        fieldError={fieldErrors && fieldErrors.email && fieldErrors.email[0]}
                    />
                    <Input
                        id={'password'}
                        name={'password'}
                        label={'Password: '}
                        type={'password'}
                        initialValue={formState.inputs.password}
                        onInput={inputHandler}
                        fieldError={fieldErrors && fieldErrors.password && fieldErrors.password[0]}
                    />
                    <Button shiny type={'submit'}>Login</Button>
                </form>
                <div className={'registration_link_container'}>
                    <span>Don't have account? <Link to={AppUrls.REGISTRATION}>Sign Up</Link></span>
                </div>
            </div>
        </div>
    );
};

export default Login;
