import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from 'react-router-dom';
import {useForm} from "../../../utils/hooks/useForm";
import Input from "../commons/form/Input/Input";
import Button from "../commons/form/Button/Button";
import {createCustomer, fetchCustomer} from "../../../store/actions/customers/customerActions";
import {useToken} from "../../../utils/hooks/useToken";
import CometSpinner from "../Spinner/CometSpinner";
import {purge} from "../../../store/actions/general/generalActions";
import Modal from "../Modal/Modal";
import CartItems from "../Cart/CartItems/CartItems";
import {useCurrency} from "../../../utils/hooks/useCurrency";

import './Checkout.css';

const initState = {
    first_name: '',
    last_name: '',
    phone: '',
    address: '',
}
const Checkout = () => {
    const [display, setDisplay] = useState(false);
    const {loading, error, fieldErrors, total, pizzas, token, customer, exchangeRate, currency, hasCustomer} = useSelector(state => ({
        loading: state.customers.loading,
        error: state.orders.error,
        fieldErrors: state.customers.fieldErrors,
        order: state.orders.order,
        total: state.carts.total,
        pizzas: state.carts.pizzas,
        token: state.auth.token,
        customer: state.customers.customer,
        exchangeRate: state.menus.rate,
        currency: state.menus.currency,
        hasCustomer: state.customers.hasCustomer
    }));
    const [formState, inputHandler, setFormData] = useForm(initState);
    const {decodeAuthToken} = useToken();
    const decoded = decodeAuthToken(token);
    const userId = (decoded && decoded.sub) || null;
    const dispatch = useDispatch();
    const {calculatePrice} = useCurrency();
    const history = useHistory();

    useEffect(() => {
        if (customer && customer.user_id === +userId) {
            const customerData = {
                first_name: customer.first_name,
                last_name: customer.last_name,
                phone: customer.phone,
                address: customer.address
            }
            console.log(customerData)
            setFormData(customerData);
        }
    }, [customer, setFormData, userId]);

    useEffect(() => {
        if (token) {
            if (!customer && userId) {
                dispatch(fetchCustomer(userId));
            }
        }
        return () => dispatch(purge());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);




    const closeModalHandler = () => {
        setDisplay(false);
    }
    const orderHandler = () => {
        const customerData = {
            ...formState.inputs,
            total: total,
            pizzas: pizzas.map(pizza => pizza.id),
            user_id: userId
        }
        dispatch(createCustomer(customerData, history));
    }

    const submitHandler = event => {
        event.preventDefault();
        setDisplay(true);
    }

    if (loading || (token && !customer && hasCustomer) || (hasCustomer &&  formState.inputs === initState)) {
        return <CometSpinner display={loading}/>
    }

    const modalActions = () => {
        return <div className={'modal_actions'}>
            <Button
                onClick={() => {
                    setDisplay(false);
                    orderHandler();
                }}
                shiny>Order</Button>
            <Button
                onClick={closeModalHandler}
                dark>Cancel</Button>
        </div>
    }

    return (
        <div className={'checkout_container'}>
            <h1>Customer Info</h1>
            {error && <h2>{error}</h2>}
            <form
                className={'order_form'}
                onSubmit={submitHandler}>
                <Input
                    id={'first_name'}
                    name={'first_name'}
                    type={'text'}
                    label={'First Name: '}
                    autoFocus
                    onInput={inputHandler}
                    initialValue={formState.inputs.first_name}
                    fieldError={fieldErrors && fieldErrors.first_name && fieldErrors.first_name[0]}
                />
                <Input
                    id={'last_name'}
                    name={'last_name'}
                    label={'Last Name: '}
                    type={'text'}
                    autoFocus
                    onInput={inputHandler}
                    initialValue={formState.inputs.last_name}
                    fieldError={fieldErrors && fieldErrors.last_name && fieldErrors.last_name[0]}
                />
                <Input
                    id={'phone'}
                    name={'phone'}
                    label={'Phone: '}
                    type={'text'}
                    onInput={inputHandler}
                    initialValue={formState.inputs.phone}
                    fieldError={fieldErrors && fieldErrors.phone && fieldErrors.phone[0]}
                />
                <Input
                    id={'address'}
                    name={'address'}
                    label={'Address: '}
                    type={'text'}
                    onInput={inputHandler}
                    initialValue={formState.inputs.address}
                    fieldError={fieldErrors && fieldErrors.address && fieldErrors.address[0]}
                />
                <Button type={'submit'}>Order</Button>
            </form>
            <Modal displayModal={display} header={'Cart Items'} footer={modalActions()}>
                <CartItems items={pizzas}/>
                <h1>Total: {parseFloat(calculatePrice(total, exchangeRate, currency)).toFixed(2)}
                    {currency === 'EUR' ? <span>&euro;</span> : <span>$</span>}</h1>
            </Modal>
        </div>
    );
};

export default Checkout;
