import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import CartItems from "../../components/Cart/CartItems/CartItems";
import Footer from "../../components/commons/Footer/Footer";
import {useCurrency} from "../../../utils/hooks/useCurrency";

import './CartOverview.css';
import Button from "../../components/commons/form/Button/Button";
import {AppUrls} from "../../../utils/appUrls";
import {clearCart} from "../../../store/actions/cart/cartActions";

const CartOverview = () => {
    const {items, total, itemsCount, currency, exchangeRate} = useSelector(state => ({
        items: state.carts.pizzas,
        total: state.carts.total,
        itemsCount: state.carts.itemsCount,
        currency: state.menus.currency,
        exchangeRate: state.menus.rate
    }));
    const dispatch = useDispatch();
    const {calculatePrice} = useCurrency();
    const totalPrice = calculatePrice(total, exchangeRate, currency).toFixed(2);

    const clearCartHandler = () => {
        dispatch(clearCart());
    }

    if (!itemsCount) {
        return <h1>Cart is empty!</h1>
    }

    return (
        <div className={'cart_overview-container'}>
            <CartItems items={items}/>
            <div>
            </div>
            {total > 0 &&
            <div className={'total_container'}>
                <h2>Total: {totalPrice}{currency === 'EUR' ? <span>&euro;</span> :
                    <span>$</span>}</h2>
            </div>}
            <div className={'cart_overview-actions'}>
                <Button
                    onClick={clearCartHandler}
                    danger>Clear Cart</Button>
                <Button to={AppUrls.CHECKOUT}>Checkout</Button>
            </div>
            <Footer/>
        </div>
    );
};

export default CartOverview;
