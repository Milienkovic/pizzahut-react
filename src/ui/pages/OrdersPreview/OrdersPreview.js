import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import CometSpinner from "../../components/Spinner/CometSpinner";
import {fetchCustomerOrders} from "../../../store/actions/orders/orderActions";
import {useToken} from "../../../utils/hooks/useToken";
import Orders from "../../components/Orders/Orders";

import './OrdersPreview.css';

const OrdersPreview = () => {
    const {loading, orders, token} = useSelector(state => ({
        loading: state.orders.loading,
        orders: state.orders.orders,
        token: state.auth.token
    }));
    const dispatch = useDispatch();
    const {decodeAuthToken} = useToken();

    useEffect(() => {
        if (token) {
            const decoded = decodeAuthToken(token)
            const userId = decoded.sub;
            dispatch(fetchCustomerOrders(userId));
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    if (loading) {
        return <CometSpinner display={loading}/>
    }

    if (!orders) {
        return <h1>No previous orders found</h1>
    }

    return (
        <div className={'orders_preview-container'}>
            <Orders orders={orders}/>
        </div>
    );
};

export default OrdersPreview;
