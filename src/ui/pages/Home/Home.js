import React from 'react';
import Menu from "../../components/Menu/Menu";

import './Home.css';

const Home = () => {
    return (
        <div className={'home_container'}>
            <Menu/>
        </div>
    );
};

export default Home;
